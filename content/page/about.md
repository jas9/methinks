---
title: About me
subtitle: Everything You Imagine is Real
comments: false
---

My name is Jas. I am a designer based in Taipei:

- I do almost all design fields
- From the time when there was no digital tools, I started graphic design
- until now I am a UI/UX designer
- The design tools have been changing
- Designers are always the same, always thinking about how to solve problems
