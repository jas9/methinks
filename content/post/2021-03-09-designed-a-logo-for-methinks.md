---
title: Designed a Logo for METHINKS
subtitle: Like a Hexagonal Honeycomb
date: 2021-03-09
tags: ["METHINKS", "logo"]
---
![METHINKS logo](https://jas9.gitlab.io/methinks/img/methinks.svg#thumbnail "METHINKS")

One advantage of the hexagon is that it can be stacked continuously.

This means that you have designed a logo, but this may only be a small part of the many graphics that will form a more complete logo in the future.

<!--more-->

六角形很合適邊鄰著邊，像蜂巢一樣建構堆疊起一個更複雜的結構圖形。