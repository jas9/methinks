## Solution-focused thinking

Designers tend to use solution conjectures as the means of developing their understanding of the problem.

Rather than accept the problem as given, designers explore the given problem and its context and may re-interpret or restructure the given problem in order to reach a particular framing of the problem that suggests a route to a solution.
